#include <iostream>
using namespace std;

int main(){
	long long int n,m,a;
	cin>>n>>m>>a;

	if(n ==0 || m == 0 || a == 0){
		cout<<0<<endl;
		return 0;
	}
	if(n%a > 0 && m%a > 0)
		cout<<((n/a) + 1)*((m/a) + 1);
	else if(n%a > 0)
		cout<<((n/a) + 1)*(m/a);
	else if(m%a > 0)
		cout<<((m/a) + 1)*(n/a);
	else
		cout<<(n/a)*(m/a);

	cout<<endl;
	return 0;
}