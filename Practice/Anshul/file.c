#include <stdio.h>
#include <stdlib.h>

int findPos(int *a, int i, int n){
	int j=0;
	for(j=i;j<n;j++){
		if(a[j] >= 0)
			return j;
	}
	return -1;
}

int findNeg(int *a, int i, int n){
        int j=0;
        for(j=i;j<n;j++){
                if(a[j] < 0)
                        return j;
        }
        return -1;
}

void Insert(int *a, int n, int k, int i){
	if(k>=i)
		return;

	int temp = a[i];
	a[i] = a[k];
	a[k] = temp;
	k++;
	Insert(a, n, k, i);
	return;
}

void AppendLast(int *a, int n, int i){
	return ;
}

void AppendFront(int *a, int n, int i){
	int k=n-1,j=0;
	for(k;k>=i;k--){
		Insert(a, n, 0, k+j);
		j++;
	}
	return;
}

void Print(int *a, int n){
	int i=0;
	for(i;i<n;i++)
		printf("%d ", a[i]);
	printf("\n");
}

int main(){
	int a[] = {-1,-3,-6,-7,2,0,-4,-1};
	// int a[] = {1,-3,-6,-7,2,0,4,1};
	int k=0,i=0,j=0,size=8;
	while(k<size){
		i = findPos(a, i, size);
		// printf("i is %d\n", i);
		if(i<0){
			AppendLast(a, size, j+1);
			break;
		}
		Insert(a, size, k, i);
		k++;
		j = findNeg(a, j, size);
		// printf("j is %d\n", j);
		if(j<0){
			AppendFront(a, size, i+1);
			break;
		}
		Insert(a, size, k, j);
		// Print(a, size);
		k++;i++;j++;
	}
	Print(a, size);
	return 0;
}
