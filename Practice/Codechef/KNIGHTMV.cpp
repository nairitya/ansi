#include <cstdio>
#include <cmath>
using namespace std;

int main(){
	int t;
	scanf("%d", &t);
	getchar();

	while(t--){
		char a[15];
		gets(a);
		
		if (a[0] <= 'h' && a[0] >= 'a' && a[3] <= 'h' && a[3] >= 'a' && a[1] <= '8' && a[1] >= '1' && a[4] <= '8' && a[4] >= '1' && a[2] == '-' && a[5] == '\0'){

			int diffx = abs(a[0] - a[3]);
			int diffy = abs(a[1] - a[4]);

			if((diffx == 2 && diffy == 1) || (diffx == 1 && diffy == 2))
				puts("Yes");
			else
				puts("No");
		}

		else
			puts("Error");
	}

	return 0;
}