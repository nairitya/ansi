#include<stdio.h>
#include<stdlib.h>
 
struct node{
    int val;
    struct node* next;
};
 
void insert(struct node** root, int val_insert)
{
    struct node* temp = (struct node*)malloc(sizeof(struct node));
    temp->val  = val_insert;
    temp->next = (*root);
    (*root) = temp;
}
 
int detect_loop(struct node *list)
{
    struct node *increse_by_one = list, *increse_by_two = list;

    while(increse_by_one && increse_by_two && increse_by_two->next ){
        increse_by_one = increse_by_one->next;
        increse_by_two  = increse_by_two->next->next;
        if(increse_by_one == increse_by_two){
            printf("Loop Detected here !!");
            return 1;
        }
        if(increse_by_one == NULL || increse_by_two == NULL){
            printf("No Loop here!!");
            return 1;
        }
    }
    return 0;
}

int main(){
    struct node* head = NULL;
 
    insert(&head, 1);
    insert(&head, 2);
    insert(&head, 3);
    insert(&head, 4);
    insert(&head, 1);
    insert(&head, 2);
    insert(&head, 3);
    insert(&head, 4);

    head->next->next->next->next->next->next = head;
    detect_loop(head);

    return 0;
}