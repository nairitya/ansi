#include <iostream>
#include <vector>
using namespace std;

int main(){
	
	vector<int> vec;
	int i;
	
	cout<<"Vector size is: "<<vec.size()<<endl;
	
	for(i=0;i<90;i++)
		vec.push_back(i);
	
	for(i=0;i<vec.size();i++)
		cout<<vec[i]<<" ";
	
	return 0;
}
