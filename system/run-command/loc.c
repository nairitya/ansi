#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main( int argc, char *argv[] )
{

  FILE *fp;
  int status;
  char path[1035];
  char cwd[100];
  char c[100] = "/bin/ls ";
  getcwd(cwd, sizeof(cwd));

  /* Open the command for reading. */
  fp = popen("/bin/ls ./run_command.c", "r");
  if (fp == NULL) {
    printf("Failed to run command\n" );
    exit;
  }
  
 
  /* Read the output a line at a time - output it. */
  while (fgets(path, sizeof(path)-1, fp) != NULL) {
    printf("%s", path);
  }

  /* close */
  pclose(fp);

  return 0;
}

