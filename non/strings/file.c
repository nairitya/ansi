#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void concat_(char *s1, char s2){
	while(*s1!='\0')
		s1++;

		*s1=s2;
		s1++;
	*s1='\0';
}

int main(){
	char *r = "abcdefgh";
	char **p;
	p = malloc(sizeof(char *)*strlen(r));
	if(p == NULL){
		printf("Error allocating memory\n");
		return 0;
	}

	int i;
	for(i=0;i<strlen(r);i++){
		p[i] = malloc(sizeof(char *)*2);
		concat_(p[i], r[i]);
		concat_(p[i], r[i]);
		printf("%s %c\n", p[i], r[i]);
	}
	for(i=0;i<strlen(r);i++){
		printf("%s", p[i]);
	}
	return 0;
}
