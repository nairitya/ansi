#include<stdio.h>
int palindrome(char*);
void main()
{
	char * str;
	printf("Enter the string to be checked");
	gets(str);
	int i=palindrome(str);
	if(i)
	printf("\n The string is palindrome");
	else
	printf("\n The string is not a palindrome");
}

int palindrome(char * str)
{
	if(str==NULL)
	return(0);
	char * end =str;
	while(*end != NULL)
	end++;
	end--;
	while(str<end)
	{
		if(*str==' ')
		str++;
		if(*end==' ')
		end--;
		if(*str != *end)
		{
		return(0);
		}
		else
		{
			str++;
			end--;
		}
	}
	return(1);
}
