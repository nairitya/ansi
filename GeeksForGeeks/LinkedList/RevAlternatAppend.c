#include <stdio.h>
#include <stdlib.h>

struct LinkedList{
	int val;
	struct LinkedList *next;
};

typedef struct LinkedList node;

void InsertAtEnd(node **root, int val){
	printf("Inserting %d at End.\n", val);
	node *temp = NULL;
	if(!(*root)){
		temp = (node *)malloc(sizeof(node));
		temp -> next = NULL;
		temp -> val = val;
		*root = temp;
		return;
	}
	temp = *root;
	while(temp -> next != NULL){
		temp = temp -> next;
	}
	node *var = (node *)malloc(sizeof(node));
	var -> next = NULL;
	var -> val = val;
	temp -> next = var;
	return;
}

void InsertAtBeg(node **root, int val){
	printf("Inserting %d at Beg.\n", val);
	node *temp = NULL;
	temp = (node *)malloc(sizeof(node));
	if(!(*root)){
		temp -> next = NULL;
        	temp -> val = val;
        	*root = temp;
        	return;
    	}
	temp -> val = val;
	temp -> next = *root;
	*root = temp;
	return;
}

void PrintEm(node *root){
	printf("Accessign me.\n");
	if(!(root))
		return;
	while(root -> next != NULL){
		printf("%d->", root->val);
		root = root -> next;
	}
	printf("%d\n", root->val);
	return;
}

void RevEm(node *root){
	node *first;
	node *second;
	// first = second = temp = (node *)malloc(sizeof(node));
	while(1){
		InsertAtBeg(&first, root->val);
		if(root -> next == NULL){
			printf("raised 1.\n");
			// InsertAtBeg(&second, root -> next -> val);
			PrintEm(first);
			PrintEm(second);
			break;
		}
		root = root -> next;
		InsertAtEnd(&second, root -> val);
		if(root -> next == NULL){
			printf("raised 2.\n");
			// InsertAtEnd(&first, root->val);
			PrintEm(first);
			PrintEm(second);
			break;
		}
		root = root -> next;
	}
	return;
}

int main(){
	node *root;
	int val;
	while(scanf("%d", &val) && val != 0){
		InsertAtEnd(&root, val);
	}
	PrintEm(root);
	RevEm(root);
	return 0;
}
