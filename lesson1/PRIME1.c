#include <stdio.h>
#include <math.h>

struct Primes {
    int a[100000];
    int size;
};
// 1000000000

struct Primes getPrimes(struct Primes P){
	int *a = P.a;
	a[0] = 2;int k =1;
	int j;
	for(long long int i=3;i<100000;i+=2){
		for(j=0;j<k;j++){
			if(i%a[j] == 0)
				break;
			if(a[i] > sqrt(i))
				break;
		}
		if(j==k){
			a[k] = i;
			k++;
		}
	}
	P.size = k;
	return P;
}

int main(){
	struct Primes P;
	P = getPrimes(P);
	int i;
	for(i=0;i<P.size;i++){
		printf("%u\n", P.a[i]);
	}
	return 0;
}