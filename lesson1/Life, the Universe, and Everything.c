#include <stdio.h>

int main()
{
	int life;
	while(scanf("%d", &life) && life != 42){
		printf("%d\n", life);
	}
	return 0;
}