#include <iostream>
#include <list>
using namespace std;

class Graph
{
  int V;
  list<int> *adj;
  public:
    Graph(int V);
    void addEdge(int v, int w);
    void DFS(int a);
    void DFS_Helper(bool *visited, int a);
};

Graph::Graph(int V)
{
  this -> V = V;
  adj = new list<int>[V];
}

void Graph::addEdge(int v, int w)
{
  adj[v].push_back(w);
}

void Graph::DFS(int a)
{
  bool *visited = new bool[V];
  for(int i=0;i<V;i++)
  visited[i] = false;
  
  DFS_Helper(visited, a);
}

void Graph::DFS_Helper(bool *visited, int a)
{
  visited[a] = true;
  cout<<a<<" ";
  list<int>::iterator i;
  
  for(i=adj[a].begin(); i != adj[a].end(); i++)
  {
    if(!visited[*i])
    {
      DFS_Helper(visited, *i);
    }
  }
}

int main()
{
  Graph g(6);
    g.addEdge(0, 1);
    g.addEdge(0, 2);
    g.addEdge(1, 2);
    g.addEdge(2, 0);
    g.addEdge(2, 4);
    g.addEdge(4, 5);
    g.addEdge(5, 3);
 
    cout << "Following is Depth First Traversal (starting from vertex 0) \n";
    g.DFS(0);
 
    return 0;
  
}