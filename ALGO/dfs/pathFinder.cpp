#include <iostream>
#include <list>
using namespace std;

class Graph
{
  int V;
  list<int> *adj;
  public:
    Graph(int V);
    void addEdge(int v, int w);
    void DFS(int a, int b);
    void DFS_Helper(bool *visited, int a, int b, list<int> stack_to_path);
};

Graph::Graph(int V)
{
  this -> V = V;
  adj = new list<int>[V];
}

void Graph::addEdge(int v, int w)
{
  adj[v].push_back(w);
}

void Graph::DFS(int a, int b)
{
  bool *visited = new bool[V];
  for(int i=0;i<V;i++)
  visited[i] = false;
  
  list<int> stack_to_path;
  
  DFS_Helper(visited, a, b, stack_to_path);
}

void Graph::DFS_Helper(bool *visited, int a, int b, list<int> stack_to_path)
{
  visited[a] = true;
  list<int>::iterator i;
  
  //cout<<a<<" ";
  if(a == b)
  {
    cout<<"Path Found !!\n";
    for(i=stack_to_path.begin(); i != stack_to_path.end(); i++)
    {
      cout<<*i<<" ";
    }
    return;
  }
  else
  {
    stack_to_path.push_back(a);
  }
  
  for(i=adj[a].begin(); i != adj[a].end(); i++)
  {
    if(!visited[*i])
    {
      DFS_Helper(visited, *i, b, stack_to_path);
      stack_to_path.pop_back();
    }
  }
}

int main()
{
  Graph g(6);
    g.addEdge(0, 1);
    g.addEdge(0, 2);
    g.addEdge(1, 2);
    g.addEdge(2, 0);
    g.addEdge(2, 4);
    g.addEdge(4, 5);
    g.addEdge(5, 3);
    
    g.DFS(0, 3);
 
    return 0;
  
}