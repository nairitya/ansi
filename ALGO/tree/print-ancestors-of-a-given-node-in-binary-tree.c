#include <stdio.h>
#include <stdlib.h>
#define true 1
#define false 0
#define bool int 

struct node{
	int data;
	struct node *left;
	struct node *right;
};

struct node *new_node(int data){
	struct node *temp = (struct node *)malloc(sizeof(struct node));
	temp->data = data;
	temp->left = NULL;
	temp->right = NULL;
	return temp;
}

bool get_ans(struct node *root, int data){
	if(root == NULL)
		return false;

	if(root->data == data)
		return true;

	if(get_ans(root->left, data) || get_ans(root->right, data)){
		printf("%d ", root->data);
		return true;
	}

	return false;
}

int main()
{
 
  /* Construct the following binary tree
              1
            /   \
          2      3
        /  \
      4     5
     /
    7
  */
  struct node *root = new_node(1);
  root->left        = new_node(2);
  root->right       = new_node(3);
  root->left->left  = new_node(4);
  root->left->right = new_node(5);
  root->left->left->left  = new_node(7);
 
  get_ans(root, 7);
 
  getchar();
  return 0;
}

