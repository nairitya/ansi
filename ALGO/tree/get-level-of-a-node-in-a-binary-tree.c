#include <stdio.h>
#include <stdlib.h>

struct node{
	int data;
	struct node *left;
	struct node *right;
};

struct node *new_node(int data){
	struct node *temp = (struct node *)malloc(sizeof(struct node));
	temp->data = data;
	temp->left = NULL;
	temp->right = NULL;
	return temp;
}

void start_travelling(struct node *root, int data, int level){
	
	if(root == NULL)
		return;

	if(root->data == data){
		printf("%d ", level);
		return;
	}

	start_travelling(root->left, data, level+1);
	start_travelling(root->right, data, level+1);
}

int main()
{
    struct node *root; 
    int x;
 
    /* Constructing tree given in the above figure */
    root = new_node(3);
    root->left = new_node(2);
    root->right = new_node(5);
    root->left->left = new_node(1);
    root->left->right = new_node(4);
 
    for (x = 1; x <=5; x++)
    {
      start_travelling(root, x, 1);
    }
 
    getchar();
    return 0;
}
