#include <stdio.h>
#include <stdlib.h>
#define MAX_SIZE 500

struct node{
	int data;
	struct node *left;
	struct node *right;
};

struct node **create_queue(int *front, int *rear){
	struct node **queue = (struct node **)malloc(sizeof(struct node)*MAX_SIZE);
	*front = *rear = 0;
	return queue;
}

struct node *new_node(int data){
	struct node *temp = (struct node *)malloc(sizeof(struct node));
	temp->data = data;
	temp->left = NULL;
	temp->right = NULL;
	return temp;
}

void enqueue(struct node **queue, int *rear, struct node *root){
	queue[*rear] = root;
	*rear += 1;
}

struct node *dequeue(struct node **queue, int *front){
	struct node *temp = queue[*front];
	*front += 1;
	return temp;
}

void print_level_order(struct node *root){
	int front, rear;
	struct node *temp = root;
	struct node **queue = create_queue(&front, &rear);

	while(temp){
		printf("%d ", temp->data);

		if(temp->left != NULL)
			enqueue(queue, &rear, temp->left);

		if(temp->right != NULL)
			enqueue(queue, &rear, temp->right);

		temp = dequeue(queue, &front);
	}
}

int main()
{
  struct node *root = new_node(1);
  root->left        = new_node(2);
  root->right       = new_node(3);
  root->left->left  = new_node(4);
  root->left->right = new_node(5); 
 
  printf("Level Order traversal of binary tree is \n");
  print_level_order(root);
 
  getchar();
  return 0;
}
