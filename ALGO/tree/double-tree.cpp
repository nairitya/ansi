#include <stdio.h>
#include <stdlib.h>

struct node{
	int data;
	struct node *left;
	struct node *right;
};

struct node *new_node(int data){
	struct node *temp = (struct node *)malloc(sizeof(struct node));
	temp->data = data;
	temp->left = NULL;
	temp->right = NULL;
	return temp;
}

void create_double_tree(struct node *root){
	
	if(root == NULL)
		return;

	struct node *temp = new_node(root->data);
	temp->left = root->left;
	root->left = temp;

	create_double_tree(root->left->left);
	create_double_tree(root->right);
}

void print_inorder(struct node *root){
	if(root == NULL)
		return;
	print_inorder(root->left);
	printf("%d ", root->data);
	print_inorder(root->right);
}

int main(){
	struct node *root = new_node(1);
	root->left        = new_node(2);
	root->right       = new_node(3);
	root->left->left  = new_node(4);
	root->left->right = new_node(5);

	print_inorder(root);
	printf("\n");
	create_double_tree(root);
	print_inorder(root);

	return 0;
}
