#include <stdio.h>
#include <stdlib.h>

struct node{
	int data;
	struct node *left;
	struct node *right;
	struct node *next;
};

struct node *new_node(int data){
	struct node *temp = (struct node *)malloc(sizeof(struct node));
	temp->data = data;
	temp->left = NULL;
	temp->right = NULL;
	temp->next = NULL;
	return temp;
}
void recur_connect(struct node *root);
struct node *find_next(struct node *root);
void connect(struct node *root){
	root->next = NULL;
	recur_connect(root);
}

void recur_connect(struct node *root){
	
	if(root == NULL)
		return;

	
	if(root->left){

		if(root->right){
			root->left->next = root->right;
			root->right->next = find_next(root);
		}

		else
			root->left->next = find_next(root);

		recur_connect(root->left);
	}
	
	else if(root->right){
		root->right->next = find_next(root);
		recur_connect(root->right);
	}

	else
		recur_connect(find_next(root));
}

struct node *find_next(struct node *root){
	struct node *temp = root->next;

	while(temp!=NULL){
		if(temp->left)
			return temp->left;
		if(temp->right)
			return temp->right;
		temp=temp->next;
	}
	return NULL;
}	

int main()
{
 
    /* Constructed binary tree is
              10
            /   \
          8      2
        /         \
      3            90
    */
    struct node *root = new_node(10);
    root->left        = new_node(8);
    root->right       = new_node(2);
    root->left->left  = new_node(3);
    root->right->right       = new_node(90);
 
    // Populates next pointer in all nodes
    connect(root);
 
    // Let us check the values of next pointers
    printf("Following are populated next pointers in the tree "
           "(-1 is printed if there is no next) \n");
    printf("next of %d is %d \n", root->data,
           root->next? root->next->data: -1);
    printf("next of %d is %d \n", root->left->data,
           root->left->next? root->left->next->data: -1);
    printf("next of %d is %d \n", root->right->data,
           root->right->next? root->right->next->data: -1);
    printf("next of %d is %d \n", root->left->left->data,
           root->left->left->next? root->left->left->next->data: -1);
    printf("next of %d is %d \n", root->right->right->data,
           root->right->right->next? root->right->right->next->data: -1);
 
    getchar();
    return 0;
}
