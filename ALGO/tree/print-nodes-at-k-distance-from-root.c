#include <stdio.h>
#include <stdlib.h>

struct node{
	int data;
	struct node *left;
	struct node *right;
};

struct node *new_node(int data){
	struct node *temp = (struct node *)malloc(sizeof(struct node));
	temp->data = data;
	temp->left = NULL;
	temp->right = NULL;
	return temp;
}

void get_k_node(struct node *root, int k){
	if(root == NULL || k<0)
		return;

	if(k==0){
		printf("%d ", root->data);
		return;
	}

	get_k_node(root->left, k-1);
	get_k_node(root->right, k-1);
}

int main()
{
 
  /* Constructed binary tree is
            1
          /   \
        2      3
      /  \    /
    4     5  8 
  */
  struct node *root = new_node(1);
  root->left        = new_node(2);
  root->right       = new_node(3);
  root->left->left  = new_node(4);
  root->left->right = new_node(5);
  root->right->left = new_node(8);  
 
  get_k_node(root, 2);
 
  getchar();
  return 0;
}

