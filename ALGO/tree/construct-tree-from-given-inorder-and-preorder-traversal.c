#include <stdio.h>
#include <stdlib.h>

struct node{
	char data;
	struct node* left;
	struct node* right;
};

struct node* new_node(char data){
	struct node *temp = (struct node *)malloc(sizeof(struct node));
	temp->data = data;
	temp->left = NULL;
	temp->right = NULL;
	return temp;
}

int search(char a[], int start, int end, char value){
	int i;
	for(i=start;i<=end;i++)
		if(a[i] == value)
			return i;
}

struct node *build_the_tree(char in[], char pre[], int start, int end){
	static int pre_index = 0;

	if(start  > end)
		return;

	struct node *temp = new_node(pre[pre_index++]);

	if(start == end)
		return temp;

	int in_index = search(in, start, end, temp->data);
	temp->left = build_the_tree(in, pre, start, in_index-1);
	temp->right = build_the_tree(in, pre, in_index+1, end);

	return temp;
}

void print_inorder(struct node *temp){
	if(temp == NULL)
		return;

	print_inorder(temp->left);
	printf(" %c ", temp->data);
	print_inorder(temp->right);
}

int main(){
	char in[] = {'D', 'B', 'E', 'A', 'F', 'C'};
	char pre[] = {'A', 'B', 'D', 'E', 'C', 'F'};
	int len = sizeof(in)/sizeof(in[0]);
	struct node *root = build_the_tree(in, pre, 0, len - 1);
 
  /* Let us test the built tree by printing Insorder traversal */
	printf("\n Inorder traversal of the constructed tree is \n");
	print_inorder(root);
	return 0;
}
