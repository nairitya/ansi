#include <stdio.h>
#include <stdlib.h>

struct node{
	int data;
	struct node *left;
	struct node *right;
};

struct node *new_node(int data){
	struct node *temp = (struct node *)malloc(sizeof(struct node));
	temp->data = data;
	temp->left = NULL;
	temp->right = NULL;
	return temp;
}

int is_same(struct node *first, struct node *second){

	if(first==NULL && second==NULL)
		return 1;

	if(first==NULL)
		return 0;

	if(second == NULL)
		return 1;

	return (first->data==second->data) && is_same(first->left, second->left) && is_same(first->right, second->right);
}

int check_sub_tree(struct node *big, struct node *small){
	if(big == NULL)
			return 0;

	if(big->data == small->data){
		if(is_same(big, small)){
			printf("True :)\n");
			return 1;
		}
	}
	return check_sub_tree(big->left, small) || check_sub_tree(big->right, small);
}

int main()
{
    /* Construct the following tree
              26
            /   \
          10     3
        /    \     \
      4      6      3
       \
        30
    */
    struct node *T        = new_node(26);
    T->right              = new_node(3);
    T->right->right       = new_node(3);
    T->left               = new_node(10);
    T->left->left         = new_node(4);
    T->left->left->right  = new_node(30);
    T->left->right        = new_node(6);
 
    /* Construct the following tree
          10
        /    \
      4      6
       \
        30
    */
    struct node *S    = new_node(10);
    S->right          = new_node(6);
    S->left           = new_node(4);
    S->left->right    = new_node(30);
 
 
    if (check_sub_tree(T, S))
        printf("Tree S is subtree of tree T");
    else
        printf("Tree S is not a subtree of tree T");
 
    getchar();
    return 0;
}
