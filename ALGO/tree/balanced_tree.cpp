#include <stdio.h>
#include <stdlib.h>

struct node
{
	int data;
	struct node* left;
	struct node* right;
};

int height(struct node* node);

bool is_balanced(struct node* root, int *height)
{
	int lh, rh, l, r;
	lh = rh = 0;

	if(root == NULL){
		*height = 0;
		return true;
	}

	l = is_balanced(root->left, &lh);
	r = is_balanced(root->right, &rh);

	*height = (lh > rh? lh:rh) + 1;

	if(abs(lh-rh)<=1 && l && r)
		return true;

	return false;
}



int max(int a, int b)
{
	return (a>=b)? a: b;
}

struct node* new_node(int data)
{
	struct node* root = (struct node*)malloc(sizeof(struct node));
	root->left = NULL;
	root->right = NULL;

	return root;
}

int height(struct node* node)
{
	if(node == NULL)
		return 0;

	return 1 + max(height(node->left), height(node->right));
}

int main()
{
	int height = 0;
	struct node* root = new_node(1);
	root->left = new_node(2);
	root->right = new_node(3);
	root->left->left = new_node(4);
	root->left->right = new_node(5);
	root->right->left = new_node(6);
	root->left->left->left = new_node(7);

	if(is_balanced(root, &height))
		printf("Tree is balanced");
	else
		printf("Tree is not balanced");

	return 0;
}
