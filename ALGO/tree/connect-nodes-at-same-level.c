#include <stdio.h>
#include <stdlib.h>

struct node{
	int data;
	struct node *left;
	struct node *right;
	struct node *next;
};

struct node *new_node(int data){
	struct node *temp = (struct node *)malloc(sizeof(struct node));
	temp -> data  = data;
	temp->left = NULL;
	temp->right = NULL;
	temp->next = NULL;
	return temp;
}
void connect_recur(struct node *root);
void connect(struct node *root){	
	root->next = NULL;
	connect_recur(root);
}

void connect_recur(struct node *root){
	
	if(!root)
		return;

	if(root->left)
		root->left->next = root->right;
	
	if(root->right)
		root->right->next = (root->next)? root->next->left: NULL;

	connect_recur(root->left);
	connect_recur(root->right);
}

int main()
{
 
  /* Constructed binary tree is
            10
          /   \
        8      2
      /
    3
  */
  struct node *root = new_node(10);
  root->left        = new_node(8);
  root->right       = new_node(2);
  root->left->left  = new_node(3);
 
  // Populates next pointer in all nodes
  connect(root);
 
  // Let us check the values of next pointers
  printf("Following are populated next pointers in the tree "
          "(-1 is printed if there is no next) \n");
  printf("next of %d is %d \n", root->data,
         root->next? root->next->data: -1);
  printf("next of %d is %d \n", root->left->data,
        root->left->next? root->left->next->data: -1);
  printf("next of %d is %d \n", root->right->data,
        root->right->next? root->right->next->data: -1);
  printf("next of %d is %d \n", root->left->left->data,
        root->left->left->next? root->left->left->next->data: -1);
 
  getchar();
  return 0;
}
