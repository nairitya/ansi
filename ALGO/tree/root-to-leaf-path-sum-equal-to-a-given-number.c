#include <stdio.h>
#include <stdlib.h>


struct node{
	int data;
	struct node *left;
	struct node *right;
};

struct node* new_node(int data){
	struct node *temp = (struct node *)malloc(sizeof(struct node));
	temp->data = data;
	temp->left = NULL;
	temp->right = NULL;
	return temp;
}

int has_path_sum(struct node *root, int sum){
	if(root == NULL)
		return sum == 0;

	int ans = 0;

	int sub_sum = sum - root->data;
	if(sub_sum == 0 && root->left == NULL && root->right == NULL)
		return 1;

	if(root->left)
		ans = has_path_sum(root->left, sub_sum);
	if(root->right)
		ans = ans || has_path_sum(root->right, sub_sum);

	return ans;
}

int main(){
	struct node *root = new_node(10);
	root->left = new_node(8);
	root->right = new_node(2);
	root->left->left = new_node(3);
	root->left->right = new_node(5);
	root->right->left = new_node(2);

	int sum;
	scanf("%d", &sum);

	if(has_path_sum(root, sum))
		printf("True\n");
	else
		printf("False\n");

	return 0;
}
