#include <stdio.h>
#include <stdlib.h>

struct node{
	int data;
	struct node *left;
	struct node *right;
};

struct node *new_node(int data){
	struct node *temp = (struct node *)malloc(sizeof(struct node));
	temp->data = data;
	temp->left = NULL;
	temp->right = NULL;
	return temp;
}

int is_sum_tree(struct node *root){
	
	if(root == NULL || (root->left == NULL && root->right == NULL))
		return 1;
	
	if(is_sum_tree(root->left) && is_sum_tree(root->right)){
		int ls = 0;
		int rh = 0;

		if(root->left == NULL)
			ls = 0;
		else if(root->left->left == NULL && root->left->right == NULL) //fucking left node is leaf node. such a motherfucker.
			ls = root->left->data;
		else
			ls = 2*(root->left->data);

		if(root->right == NULL)
			rh = 0;
		else if(root->right->left==NULL && root->right->right==NULL)
			rh = root->right->data;
		else
			rh = 2*(root->right->data);

		if(root->data == ls+rh)
			return 1;
		return 0;
	}
	
	return 0;
}

int main()
{
    struct node *root  = new_node(26);
    root->left         = new_node(10);
    root->right        = new_node(3);
    root->left->left   = new_node(4);
    root->left->right  = new_node(6);
    root->right->right = new_node(3);
    if(is_sum_tree(root))
        printf("The given tree is a SumTree ");
    else
        printf("The given tree is not a SumTree ");
 
    getchar();
    return 0;
}
