#include <stdio.h>
#include <stdlib.h>

struct node{
	int data;
	struct node* left;
	struct node* right;
};

struct node* new_node(int data)
{
	struct node* temp = (struct node*)malloc(sizeof(struct node));
	temp->data = data;
	temp->left = NULL;
	temp->right = NULL;
	return temp;
}

void morris_travel(struct node* root){
	struct node *current, *pre;

	if(root == NULL)
		return;

	current = root;
	
	while(current != NULL){
		
			if(current->left == NULL){
				printf(" %d ", current->data);
				current = current->right;
			}
			else{
				pre = current->left;
				while(pre->right != NULL && pre->right != current){
					pre = pre->right;
				}
				if(pre->right == NULL){
					pre->right = current;
					current = current->left;
				}
				else
				{
					printf(" %d ", current->data);
					pre->right = NULL;
					current = current->right;
				}

			}
	}
}
int main()
{
	struct node* root = new_node(1);
	root->left = new_node(2);
	root->right = new_node(3);
	root->left->left = new_node(4);
	root->left->right = new_node(5);
	morris_travel(root);
	return 0;
}
