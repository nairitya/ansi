#include <cstdio>
#include <cstdlib>
#include <list>
using namespace std;

struct node{
	int data;
	struct node *left;
	struct node *right;
};

struct node *new_node(int data){
	struct node *temp = (struct node *)malloc(sizeof(struct node));
	temp->data = data;
	temp->left = NULL;
	temp->right = NULL;
	return temp;
}

void travel(struct node *root, list<int> stack){
	
/*	if(root == NULL){
		list<int>::iterator i;
		for(i=stack.begin();i!=stack.end();i++){
			printf("printing .. ");
			printf("%d ", *i);
		}
		printf("\n");
		return;
	}*/

	if(root->left){
		stack.push_back(root->left->data);
		travel(root->left, stack);
		stack.pop_back();
	}

	if(root->right){
		stack.push_back(root->right->data);
		travel(root->right, stack);
		stack.pop_back();
	}
	
	if(!root->left && !root->right)
	{
		list<int>::iterator i;
		for(i=stack.begin();i!=stack.end();i++){
			printf("%d ", *i);
		}
		printf("\n");
		return;
	}

}

int main(){
	struct node *root = new_node(10);
	root->left = new_node(8);
	root->right = new_node(2);
	root->left->left = new_node(3);
	root->left->right = new_node(5);
	root->right->left = new_node(2);
	list<int> stack;
	stack.push_back(root->data);
	travel(root, stack);
	return 0;
}
