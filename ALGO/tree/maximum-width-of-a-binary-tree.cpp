#include <stdio.h>
#include <stdlib.h>

struct node{
	int data;
	struct node *left;
	struct node *right;
};

int level_helper(struct node *root, int *count, int level);
int get_max(int *count, int h);

struct node *new_node(int data){
	struct node *temp = (struct node *)malloc(sizeof(struct node));
	temp->data = data;
	temp->left = NULL;
	temp->right = NULL;
	return temp;
}

int max(int a, int b){
	return (a>b)? a: b;
}

int height(struct node *root){
	if(root == NULL)
		return 0;
	return max(1+height(root->left), 1+height(root->right));
}

int get_width(struct node *root){
	int h = height(root);
	int *count = (int *)malloc(sizeof(int)*h);
	int level = 0;

	level_helper(root, count, level);

	return get_max(count, h);
}

int level_helper(struct node *root, int *count, int level){
	if(root){
		count[level]++;
		level_helper(root->left, count, level+1);
		level_helper(root->right, count, level+1);
	}
}

int get_max(int *count, int h){
	int high = count[0];
	for(int i=1;i<h;i++)
		if(count[i] > high)
			high = count[i];
	return high;
}

int main()
{
  struct node *root = new_node(1);
  root->left        = new_node(2);
  root->right       = new_node(3);
  root->left->left  = new_node(4);
  root->left->right = new_node(5);
  root->right->right = new_node(8);
  root->right->right->left  = new_node(6);
  root->right->right->right  = new_node(7);
 
  printf("Maximum width is %d \n", get_width(root));
  return 0;
}
