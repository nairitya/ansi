#include <iostream>
#include <list>

using namespace std;

class Graph
{
    int V;
    list<int> *adj;
 public:
     Graph(int V);
     void addEdge(int v, int w);
     void BFS(int s);
 };
 
 Graph::Graph(int V)
 {
     this->V = V;
     adj = new list<int>[V];
 }
 
void Graph::addEdge(int v, int w)
{
    adj[v].push_back(w);
}

void Graph::BFS(int s)
{
    bool *visited = new bool[V];
    for(int i=0; i<V; i++)
        visited[i] = false;
        
    list<int> queue;
    visited[s] = true;
    
    queue.push_back(s);
    cout<<"I\n";
    list<int>::iterator i;
    while(!queue.empty()){
      cout<<"II\n";
        s = queue.front();
        cout<<s<<" ";
        queue.pop_front();
        cout<<"III\n";
        
        for(i = adj[s].begin(); i!=adj[s].end(); i++){
          
          if(i == adj[s].end())
          break;
            cout<<"IV"<<*i<<"\n";
            if(!visited[*i]){
                visited[*i] = true;
                queue.push_back(*i);
            }
        }        
    }    
}                 

int main()
{
    int t,n,i,m,temp,a,b;
    cin>>t;
    
    while(t--)
    {
        cin>>n;
        Graph g(n);
        int templ = n;
        
        for(templ=n;templ>0;templ--){
                cin>>i>>m;
                while(m--)
                {                
                                cin>>temp;
                                g.addEdge(i, temp);
                }
        }    
        
        cin>>a>>b;
        while(1)
        {
            if(a==0 && b==0)
            break;
            g.BFS(a);
            cin>>a>>b;
        }    
        
    }
    return 0;
}    
