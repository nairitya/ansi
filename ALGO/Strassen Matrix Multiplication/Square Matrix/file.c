#include "stdio.h"

int** MakeMatrix(int n){
	int** Matrix = malloc(sizeof(*Matrix)*n);
	int i;
	for(i=0;i<n;i++){
		Matrix[i] = malloc(sizeof(**Matrix)*n);
	}
	return Matrix;
}

int** Multiply(int (*A)[3], int (*B)[3], int n){
	int** C = MakeMatrix(n);
	int i,j,k;
	for(i=0;i<n;i++){
		for(j=0;j<n;j++){
			printf("%d\n", A[i][j]);
			C[i][j] = 0;
			for(k=0;k<n;k++){
				C[i][j] = C[i][j] + A[i][k]*B[k][j];
			}
		}
	}
	return C;
}

int main(){
	int size = 3;
	int A[3][3] = {1,2,3,4,5,6,7,8,9},
		B[3][3] = {1,0,0,0,1,0,0,0,1};
	// int** C = MakeMatrix(size);
	int** C = Multiply(A, B, size);
	int i,j;
	for (i = 0; i < size; ++i)
	{
		for (j = 0; j < size; ++j)
		{
			printf("%d ", C[i][j]);
		}
		printf("\n");
	}
	return 0;
}