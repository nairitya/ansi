#include "stdio.h"

struct tempData{
	int maxLeft, maxRight, sum;
};

struct tempData MaxSubArrayMid(int *A, int low, int mid, int high){
	struct tempData T;
	int sum =0, i, rightSum, leftSum;
	leftSum = rightSum = -999999; // -infinity;
	for(i=mid; i>=low; i--){
		sum = sum + A[i];
		if(sum > leftSum){
			leftSum = sum;
			T.maxLeft = i;
		}
	}
	sum = 0;
	for(i=mid+1;i<high;i++){
		sum = sum + A[i];
		if(sum > rightSum){
			rightSum = sum;
			T.maxRight = i;
		}
	}
	T.sum = leftSum + rightSum;
	return T;
}

struct tempData MaxSubArraySides(int *A, int low, int high){
	struct tempData T;
	if(low == high){
		T.maxLeft = T.maxRight = low;
		T.sum = A[low];
		return T;
	}
	else{
		struct tempData T_Right, T_Cross;
		int mid = (low+high)/2;
		T = MaxSubArraySides(A, low, mid);
		T_Right = MaxSubArraySides(A, mid+1, high);
		T_Cross = MaxSubArrayMid(A, low, mid, high);
		if(T.sum > T_Right.sum && T.sum > T_Cross.sum)
			return T;
		else if(T_Right.sum > T_Cross.sum)
			return T_Right;
		else
			return T_Cross;
	}
}

int main(){
	int Data[] = {13,-3,-25,20,-3,-16,-23,18,20,-7,12,-5,-22,15,-4,7};
	struct tempData T = MaxSubArraySides(Data, 0, 16);
	printf("%d %d %d\n", T.maxLeft, T.maxRight, T.sum);
	return 0;
}