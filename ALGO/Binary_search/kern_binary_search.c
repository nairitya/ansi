#include <stdio.h>

unsigned int buff[] = {1,2,3,4,5,6,7};
unsigned int buffsize = 7;
static unsigned int binary_search(unsigned int addr){
	unsigned int first, last, middle;
	first = 0; last = buffsize-1; middle = (first+last)/2;
	while(first < last){
		if(addr == buff[middle])
			return 1;
		else if(addr > buff[middle]){
			first = middle + 1;
			middle = (first+last)/2;
		}
		else{
			last = middle - 1;
			middle = (first+last)/2;
		}
	}
	return 0;
}

int main(){
	printf("%u\n", binary_search(0));
	return 0;
}
