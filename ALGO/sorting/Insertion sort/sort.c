#include "stdio.h"

int main(){
	int sizeofArray, key, i, j;
	printf("Enter the size of array to be inserted : ");
	scanf("%d", &sizeofArray);
	int *data = malloc(sizeof(*data) * sizeofArray);
	
	printf("Start entering the numbers : ");
	for(i=0;i<sizeofArray;i++)
		scanf("%d", &data[i]);

	printf("Sorting started \n");
	for(j=1;j<sizeofArray;j++){
		key = data[j];
		i = j-1;
		while(i>=0 && data[i] > key){
			data[i+1] = data[i];
			i--;
		}
		data[i+1] = key;
	}
	
	for(j=0;j<sizeofArray;j++){
		printf("%d ", data[j]);
	}
	printf("\n");
	return 0;
}