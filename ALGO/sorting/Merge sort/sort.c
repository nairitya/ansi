#include "stdio.h"

void Merge(int *A, int p, int q, int r){
	int n1 = q-p+1;
	int n2 = r-q;
	int *L = malloc(sizeof(*L)*(n1+1));
	int *R = malloc(sizeof(*R)*(n2+1));
	int i,j,k;
	
	for(i=0;i<n1;i++)
		L[i] = A[p+i];
	
	for(j=0;j<n2;j++)
		R[j] = A[q+j+1];
	
	L[n1] = R[n2] = 9999999;
	i = j = 0;
	
	for(k=p;k<=r;k++){
		if(L[i] < R[j]){
			A[k] = L[i];
			i++;
		}
		else{
			A[k] = R[j];
			j++;
		}
	}
}

void Divide(int *A, int q, int r){
	int p;
	if(q<r){
		p = (q+r)/2;
		Divide(A, q, p);
		Divide(A, p+1, r);
		Merge(A, q, p, r);
	}
}

int main(){
	int a[] = {1,4,5, 8, 12,3,4,43,6,6,32,33,5,5,643,2,3454,65,675643,2,3454,65,43,5,46,554,34,32,3454,2,3 ,6,7, 1,3,5,7,8,6,5432,1,13,4}, i;
	int size = sizeof(a)/sizeof(int);
	Divide(a, 0, size-1);
	for(i=0;i<size;i++){
		printf("%d\n", a[i]);
	}
	return 0;
}