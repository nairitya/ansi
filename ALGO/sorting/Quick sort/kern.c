#include <stdio.h>

static unsigned int a[] = {7,6,5,4,3,2,1};
static unsigned int buffsize = 7;

void swap(unsigned int *a, unsigned int *b){
	unsigned int temp = *a;
	*a = *b;
	*b = temp;
}

unsigned int partition(unsigned int start, unsigned int end){
	unsigned int pivot = a[end], index = start, i=start;
	while(i<end){
		if(a[i] <= pivot){	
			swap(&a[i], &a[index]);
			index++;
		}
		i++;
	}
	swap(&a[index], &a[end]);
	return index;
}

void Quick_sort (unsigned int start, unsigned int end){
	unsigned int index;
	if(start < end){
		index = partition(start, end);
		Quick_sort(start, index-1);
		Quick_sort(index+1 , end);
	}
	return;
}

int main()
{
    unsigned int i;
    Quick_sort(0,buffsize-1);
    for(i=0;i<buffsize;i++)
    printf("%u ", a[i]);
    return 0;
}
