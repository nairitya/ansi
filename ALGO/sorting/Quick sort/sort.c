#include <stdio.h>
#include <stdlib.h>

void Quicksort(int *a, int first, int last){
	int pivot, i, j, temp;
	if(first<last){
		pivot = first;
		i = first;
		j = last;
		while(i<j){
			while(a[i]<=a[pivot] && i<last){
				i++;
			}
			while(a[j]>=a[pivot]){
				j--;
			}
		
			if(i<j){
				temp = a[i];
				a[i] = a[j];
				a[j] = temp;
			}
		}
		temp = a[pivot];
		a[pivot] = a[j];
		a[j] = temp;
		Quicksort(a, first, j-1);
		Quicksort(a, j+1, last);
	}	
		
}

void print(int *a, int size){
	int i=0;
	for(i;i<size;i++)
		printf("%d  ", a[i]);
}

int main(){
	int a[] = {1 ,3 ,4, 6, 8 , 9, 5, 0, 12, 21, 13, 4, 12};
	int size = sizeof(a)/sizeof(int);
	Quicksort(a, 0, size-1);
	print(a, size);
	return 0;
}
