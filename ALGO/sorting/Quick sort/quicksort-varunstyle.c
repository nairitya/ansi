#include <stdio.h>
#include <stdlib.h>

void swap(int *a, int *b){
	int temp = *a;
	*a = *b;
	*b = temp;
}

void Quick(int *a, int first, int last){
	printf("first is %d, last is %d\n", first, last);
	if(first < last){
		int pivot = first, i, j;
		i = j = first+1;
		while(i <= last){
			if(a[i] < a[pivot]){
				swap(&a[i], &a[j]);
				j++;
			}
			i++;
		}
		swap(&a[pivot], &a[j-1]);
	Quick(a, first, j);
	Quick(a, j+1, last);
	}
}

void print(int *a, int size){
	int i=0;
	for(i;i<size;i++)
		printf("%d  ", a[i]);
}

int main(){
	int a[] = {1 ,3 ,4, 6, 8 , 9, 5, 0, 12, 21, 13, 4, 12};
	int size = sizeof(a)/sizeof(int);
	Quick(a, 0, size-1);
	print(a, size);
	return 0;
}

