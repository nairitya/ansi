#include <stdio.h>

unsigned int partition(unsigned int *a, unsigned int start, unsigned int end){
	unsigned int pivot = a[end], index = start, i=0, temp;
	while(i<end){
		if(a[i] < pivot){	
			temp = a[i];
			a[i] = a[index];
			a[index] = temp;
			index++;
		}
		i++;
	}
	temp = a[index];
	a[index] = a[end];
	a[end] = temp;
	return index;
}

void Quick_sort(unsigned int *a, unsigned int start, unsigned int end){
	unsigned int index;
	if(start < end){
		index = partition(a, start, end);
		Quick_sort(a, start, index-1);
		Quick_sort(a, index+1 , end);
	}
	return;
}

int main()
{
    unsigned int n, i;
    scanf("%u", &n);
    unsigned int a[n];
    for(i=0;i<n;i++)
    scanf("%u", &a[i]);
    Quick_sort(a,0,n-1);
    for(i=0;i<n;i++)
    printf("%u ", a[i]);
}
