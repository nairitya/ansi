#include <stdio.h>

static unsigned int buff[] = {9, 8, 7, 6, 5, 4, 3, 2, 1};
static unsigned int buffsize = 9;


unsigned int partition(unsigned int *,  int ,  int );
void Quick_sort(unsigned int *,  int ,  int );
void _swap_(unsigned int *, unsigned int *);
/**/

static void printq(){
	unsigned int i;
	for(i=0;i<buffsize;i++)
		printf("%u ", buff[i]);
}

unsigned int partition(unsigned int *a, int start, int end){
	printf("start and end are %u %u\n", start, end);
	unsigned int pivot;
	int index, i;
	i = index = start;
	pivot = a[end];
	while(i < end){
		if(a[i] <= pivot){	
			_swap_(&(a[i]), &(a[index]));
			index++;
		}
		i++;
	}
	_swap_(&(a[index]), &(a[end]));
	printf("Returned index value is : %u, a[index] is %u\n", index, a[index]);
	return index;
}

void Quick_sort(unsigned int *a, int start, int end){
	if(start < end){
		int index = partition(a, start, end);
		Quick_sort(a, start, index-1);
		Quick_sort(a, index+1 , end);
	}
	else{
		return;
	}
}


void _swap_(unsigned int *i, unsigned int *j){
	unsigned int temp;
	temp = (*i);
	(*i) = (*j);
	(*j) = temp;
}

int main(){
	Quick_sort(buff, 0, buffsize-1);
	printq();
	return 0;
}
