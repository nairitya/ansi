#include <iostream>
using namespace std;
int partition(int a[],int start,int end)
{
    int pivot=a[end];
    int pIndex=start;
    for(int i=start;i<end;i++)
    {
        if(a[i]<=pivot)
        {
            swap(a[i],a[pIndex]);
            pIndex++;
        }
    }
    swap(a[pIndex],a[end]);
    return pIndex;
}
void QuickSort(int a[],int start,int end)
{
    if(start<end)
    {
        int pIndex=partition(a,start,end);
        QuickSort(a,start,pIndex-1);
        QuickSort(a,pIndex,end);
    }
}
int main()
{
    int n;
    cout<<"Enter The Size Of Array To Sort : ";
    cin>>n;
    int a[n];
    for(int i=0;i<n;i++)
    {
        cin>>a[i];
    }
    QuickSort(a,0,n-1);
    cout<<"Sorted Array is : \n";
    for(int i=0;i<n;i++)
    {
        cout<<a[i]<<"  ";
    }
}
