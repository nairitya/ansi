#include <stdio.h>

struct node{
    struct node *leftChild, *rightChild;
    int data, index;
};

struct node init(){
    struct node N;
    return N;
};

struct node Insert(struct node root, int val){
	struct node temp;
	#ifndef root
		printf("It is NULL.\n");
        root.data = val;
        temp = root;
    #endif
	return temp;
};

struct node Delete(struct node root, int val){
	struct node temp;
	return temp;
};

int main(){
    struct node root = init();
    int y, val;
    printf("0. Exit\n1. Insertion\n2. Deletion\n");
    do{
        scanf("%d", &y);
        switch(y){
        	case 0: printf("Exiting Now .....\n");
        			break;
            case 1: scanf("%d", &val);
                    Insert(root, val);
                    break;
            case 2: scanf("%d", &val);
                    Delete(root, val);
                    break;
            default: printf("You've been sleeping a lot.\n");
        }
    }while(y!=0);
    return 0;
}
