#include <stdio.h>
#include <stdlib.h>

struct bin_tree{
	int data;
	struct bin_tree *right, *left;
};

typedef struct bin_tree node;

void insert(node **tree, int val){
	node *temp = NULL;
	if(!(*tree)){
		temp = (node *)malloc(sizeof(node));
		temp->left = temp->right = NULL;
		*tree = temp;
		return;
	}
	
	if(val < (*tree)->data){
		insert(&(*tree)->left, val);
	}
	else{
		insert(&(*tree)->right, val);
	}
}

node* search(node ** tree, int val){
	if(!(*tree)){
		return NULL;
	}
	if(val == (*tree)->data){
		return (*tree);
	}
	else if(val < (*tree)->data){
		search(&(*tree)->left, val);
	}
	else{
		search(&(*tree)->right, val);
	}
}

void preorder(node *tree){
	if(!tree)
		return;
	printf("%d\n", tree->data);
	preorder(tree, tree->left);
	preorder(tree, tree->right);
}

void inorder(node *tree){
	if(!tree)
		return ;
	inorder(tree->left);
	printf("%d", tree->data);
	inorder(tree->right);
}

void postorder(node *tree){
	postorder(tree->left);
	postorder(tree->right);
	printf("%d", tree->data);
}

void deltree(node *tree){
	if(tree){
		deltree(tree->left);
		deltree(tree->right);
		free(tree);
	}
}


