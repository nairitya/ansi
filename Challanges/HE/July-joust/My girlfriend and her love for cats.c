#include "stdio.h"
#include <stdlib.h>
void Merge(int *A, int p, int q, int r){
	int n1 = q-p+1;
	int n2 = r-q;
	int *L = malloc(sizeof(*L)*(n1+1));
	int *R = malloc(sizeof(*R)*(n2+1));
	int i,j,k;
	
	for(i=0;i<n1;i++)
		L[i] = A[p+i];
	
	for(j=0;j<n2;j++)
		R[j] = A[q+j+1];
	
	L[n1] = R[n2] = 9999999;
	i = j = 0;
	
	for(k=p;k<=r;k++){
		if(L[i] < R[j]){
			A[k] = L[i];
			i++;
		}
		else{
			A[k] = R[j];
			j++;
		}
	}
}

void Divide(int *A, int q, int r){
	int p;
	if(q<r){
		p = (q+r)/2;
		Divide(A, q, p);
		Divide(A, p+1, r);
		Merge(A, q, p, r);
	}
}

int main(){
	int NoCat;
	scanf("%d", &NoCat);
	int *cal = malloc(sizeof(*cal)*NoCat);
	int *energy = malloc(sizeof(*energy)*NoCat);
	int i;
	for(i=0;i<NoCat;i++)
		scanf("%d", &cal[i]);
	for(i=0;i<NoCat;i++)
		scanf("%d", &energy[i]);
	Divide(cal, 0, NoCat-1);
	Divide(energy, 0, NoCat-1);
	long long int sum = 0;
	for(i=0;i<NoCat;i++){
		sum += cal[i]*energy[i];
	}
	printf("%llu", sum);
	return 0;
}