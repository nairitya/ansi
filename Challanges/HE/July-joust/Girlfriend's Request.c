#include "stdio.h"
#include <stdlib.h>
void Merge(int *A, int p, int q, int r){
	int n1 = q-p+1;
	int n2 = r-q;
	int *L = malloc(sizeof(*L)*(n1+1));
	int *R = malloc(sizeof(*R)*(n2+1));
	int i,j,k;
	
	for(i=0;i<n1;i++)
		L[i] = A[p+i];
	
	for(j=0;j<n2;j++)
		R[j] = A[q+j+1];
	
	L[n1] = R[n2] = 9999999;
	i = j = 0;
	
	for(k=p;k<=r;k++){
		if(L[i] < R[j]){
			A[k] = L[i];
			i++;
		}
		else{
			A[k] = R[j];
			j++;
		}
	}
}

void Divide(int *A, int q, int r){
	int p;
	if(q<r){
		p = (q+r)/2;
		Divide(A, q, p);
		Divide(A, p+1, r);
		Merge(A, q, p, r);
	}
}

int main(){
	int N,i=0,k,j,len;
	scanf("%d", &N);
	int *A = malloc(sizeof(*A)*N);
	for(i=0;i<N;i++)
		scanf("%d", &A[i]);
	int a,b,Q;
	scanf("%d", &Q);
	i = 0;
	while(i<Q){
		scanf("%d%d", &a,&b);
		len = b-a+1;
		int *B = malloc(sizeof(*B)*len);
		j=0;
		for(k=a-1;k<=b-1;k++){
			B[j] = A[k];
			j++;
		}
		Divide(B, 0, len-1);
		if(len % 2 == 0)
			printf("%d\n", B[(len/2)-1]);
		else
			printf("%d\n", B[len/2]);
		free(B);
		i++;
	}
	free(A);
	return 0;
}