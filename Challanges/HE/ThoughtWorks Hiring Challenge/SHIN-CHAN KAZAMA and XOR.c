#include "stdio.h"

int XOR(int A, int B){
	return A^=B;
}

int main(){
	int N, I, r, X, Q, i=0, Z, count=0, less;
	scanf("%d", &N);
	int *A = malloc(sizeof(*A)*N);

	for(i;i<N;i++)
		scanf("%d", &A[i]);

	scanf("%d", &Q);
	int j=0;
	printf("\n");
	while(j<Q){
		scanf("%d%d%d", &I, &r, &X);
		count = 0;
		less = XOR(A[I],X);
		for(i=I-1;i<r;i++){
			int temp = XOR(A[i],X);
			if(temp < less){
				Z = A[i];
				less = temp;
				count = 1;
			}
			else if(temp == less){
				count++;
				Z = A[i];
			}
			// printf("Z is %d, less is %d, count is %d, temp is %d, A[i] is %d\n", Z, less, count, temp, A[i]);
		}
		printf("%d %d\n", Z, count);
		j++;
	}
	return 0;	
}
