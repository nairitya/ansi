#include <stdio.h>
#define MAX_LEN 100

int count(char *p, char *vowel, int len){
	int i,j=0,countP=0;
	for(i=0;i<len;i++){
		for(j=0;j<5;j++){
			if(p[i] == vowel[j])
				countP++;
		}
	}
	return countP;
}

int main(){
	int t,i =0;
	long unsigned int len, klen;
	char vowel[5] = {'a','e','i','o','u'};
	char *site = malloc(sizeof(char)*MAX_LEN);

	scanf("%d", &t);
	while(i<t){
		scanf("%s", site);
		len = strlen(site);
		klen = len - count(site, vowel, len) - 3;
		printf("%lu/%lu", klen, len);
		i++;
	}
	return 0;
}
