#include <stdio.h>

int GCD(int a, int b){
	if(a==0)
		return b;
	else
		return GCD(b%a, a);
}

void StartGame(int a, int b, int turn){
	int temp;
	switch(turn){
		case 1:
			if(a<=1 && b<=1){
				printf("Draw\n");
				return;
			}
			if(a<=1){
				printf("Chandu Don\n");
				return;
			}
			temp = GCD(a, b);
			if(temp == 1)
				b-=1;
			else if(GCD(b/temp, a) != 1)
				b-=1;
			else
				b/=temp;
			StartGame(a, b, 0);
			break;
		case 0:
			if(b<=1){
				printf("Arjit\n");
				return;
			}
			temp = GCD(a, b);
			if(temp == 1)
				a-=1;
			else if(GCD(a/temp, b) != 1)
				a-=1;
			else
				a/=temp;
			StartGame(a, b, 1);
			break;
	}
}

int main(){
	int t, a, b, i;
	scanf("%d", &t);
	while(i<t){
		scanf("%d%d",&a,&b);
		StartGame(a, b, 1);
		i++;
	}
	return 0;
}
