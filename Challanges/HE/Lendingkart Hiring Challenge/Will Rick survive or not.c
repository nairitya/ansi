#include <stdio.h>
#include <stdlib.h>

void merge(int *arr,int min,int mid,int max)
{
  int tmp[100002];
  int i,j,k,m; 
  j=min;
  m=mid+1;
  for(i=min; j<=mid && m<=max ; i++)
  {
     if(arr[j]<=arr[m])
     {
         tmp[i]=arr[j];
         j++;
     }
     else
     {
         tmp[i]=arr[m];
         m++;
     }
  }
  if(j>mid)
  {
     for(k=m; k<=max; k++)
     {
         tmp[i]=arr[k];
         i++;
     }
  }
  else
  {
     for(k=j; k<=mid; k++)
     {
        tmp[i]=arr[k];
        i++;
     }
  }
  for(k=min; k<=max; k++)
     arr[k]=tmp[k];
}

void part(int *arr,int min,int max)
{
 int mid;
 if(min<max)
 {
   mid=(min+max)/2;
   part(arr,min,mid);
   part(arr,mid+1,max);
   merge(arr,min,mid,max);
 }
}

int main(){
	int t, n, i=0,a[100005];
	scanf("%d", &t);
	while(i<t){
		scanf("%d", &n);
		int j=0, count=0, Dead = 0;
		for(j;j<n;j++)	scanf("%d", &a[j]);		
		part(a, 0, n-1);
		for(j=0;j<n;j++){
			
			if(j%6 == 0 && j != 0)
                                count--;
            if(a[j] <= (-1)*count){
				Dead = 1;
				break;
			}
         count--;
                                
                        
		}
		if(Dead)
			printf("Goodbye Rick\n%d\n",j);
		else
			printf("Rick now go and save Carl and Judas\n");
		i++;
	}
	return 0;
}
