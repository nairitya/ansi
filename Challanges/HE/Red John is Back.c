#include <stdio.h>

void OUTPUT(int n){
	int M;
	int DP = malloc(sizeof(n)*n);
	M = totalConfiguration(DP, n);
	printf("%d\n", M);
}

int totalConfiguration(int *DP, int n){
	switch(n){
		case 0:	DP[0] = 0;
			return DP[0];
		case 1: DP[1] = 1;
			return DP[1];
		case 2: DP[2] = 1;
			return DP[2];
		case 3: DP[3] = 1;
			return DP[3];
		case 4:	DP[4] = 2;
			return DP[4];
		default: DP[n] = totalConfiguration(DP, n-1) + totalConfiguration(DP, n-4);
			return DP[n];
	}
}

int main(){
	int timeCases;
	int N;
	scanf("%d", &timeCases);
	int i=0;
	while(i<timeCases){
		scanf("%d", &N);
		OUTPUT(N);
		i++;
	}
	return 0;
}
