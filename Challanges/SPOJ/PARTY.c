#include <stdio.h>
#include <stdlib.h>

int max(int a, int b){
	return a>b ? a: b;
}

int min(int a, int b){
	return a<b ? a: b;
}

int Process(int n, int W, int *Ni, int *Pi){
	int i, w;
	int K[n+1][W+1];
	for (i = 0; i <= n; i++){
		for (w = 0; w <= W; w++){
			if (i==0 || w==0)
				K[i][w] = 0;
			else if (Ni[i-1] <= w)
				K[i][w] = max(Pi[i-1] + K[i-1][w-Ni[i-1]],  K[i-1][w]);
			else
				K[i][w] = K[i-1][w];
		}
	}
	return K[n][W];
}

int main(){
	int N, P;
	while(scanf("%d%d", &N, &P) && N != 0 && P != 0){
		int *Ni = malloc(sizeof(*Ni)*P);
		int *Pi = malloc(sizeof(*Pi)*P);
		int i=0;
		for(i;i<P;i++)
			scanf("%d%d", &Ni[i], &Pi[i]);
		int max = Process(P, N, Ni, Pi);
		printf("%d\n", max);
	}
	return 0;
}
