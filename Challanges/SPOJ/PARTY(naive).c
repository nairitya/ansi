#include <stdio.h>
#include <stdlib.h>

int max(int a, int b){
	return a>b ? a: b;
}

int Process(int n, int W, int *Ni, int *Pi){
	if(n ==0 || W==0)
		return 0;
	if(Ni[n-1] > W)
		return Process(n-1, W, Ni, Pi);
	else
		return max(Pi[n-1] + Process(n-1, W-Ni[n-1], Ni, Pi), Process(n-1, W, Ni, Pi));	
}

int main(){
	int N, P;
	while(scanf("%d%d", &N, &P) && N != 0 && P != 0){
		int *Ni = malloc(sizeof(*Ni)*P);
		int *Pi = malloc(sizeof(*Pi)*P);
		int i=0;
		for(i;i<P;i++)
			scanf("%d%d", &Ni[i], &Pi[i]);
		int got = Process(P, N, Ni, Pi);
		printf("%d\n", got);
	}
	return 0;
}
