#include <stdio.h>
#include <stdlib.h>

int main(){
	long long int t,n,i=0;
	scanf("%llu", &t);
	while(i<t){
		scanf("%llu", &n);
		long long int *candy = malloc(sizeof(*candy)*n); 
		long long int j = 0, bal=0;
		for(j;j<n;j++){
			scanf("%llu", &candy[i]);
			bal += candy[i];
			if(bal > n)
				bal %= n;
		}
		if(bal == 0)
			printf("YES\n");
		else
			printf("NO\n");
		i++;
	}
	return 0;
}
