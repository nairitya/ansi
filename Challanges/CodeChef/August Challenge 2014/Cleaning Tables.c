#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int alreadyOccupied(int *a, int n, int num){
	int i=0;
	for(i;i<n;i++){
		
		if(a[i] == num)
			return 1;
	}
	return 0;
}

int withOneOrder(int *a, int n, int *b){
	int i=0, min = i;
	for(i;i<n;i++){
		if(b[a[min]] > b[a[i]])
			min = i;

		printf("b[%d] is %d\n",a[i], b[a[i]]);
		if(b[a[i]] == 0){
			printf("Yeah found %d\n", b[a[i]]);
			return i;
		}
	}
	return min;
}

int main(){
	int T,N,M, i=0;
	scanf("%d", &T);
	while(i<T){
		scanf("%d%d", &N, &M);
		int *seq = malloc(sizeof(*seq)*M);
		int *count = malloc(sizeof(*count)*M);
		int *isSeating = malloc(sizeof(*isSeating)*N);
		memset(count, 0, M);
		memset(isSeating, 0, N);
		// count[M] = {0};
		// isSeating[N] = {0};
		int counter = 0, j = 0, locator = 0, totalSeating = 0;
		for(j;j<M;j++){
			scanf("%d", &seq[j]);
			count[seq[j]]++;			
			printf("setting count[%d] to %d\n", seq[j], count[seq[j]]);
		}
		for(j=0;j<M;j++){
			if(alreadyOccupied(isSeating, N, seq[j]) == 0){
				if(totalSeating == N){
					locator = withOneOrder(isSeating, N, count);//Replace the one with one order!
					printf("%d is removed from table %d, %d is seating now !!\n", isSeating[locator], locator, seq[j]);
					isSeating[locator] = seq[j];
					counter++;
					count[seq[j]]--;
				}
			
				else{
					printf("%d is seating in table %d\n", seq[j], locator);
					isSeating[locator] = seq[j];
					locator++;
					counter++;
					count[seq[j]]--;
					totalSeating++;
				}
			}
			else{
				printf("%d is already sitting \n", seq[j]);
				count[seq[j]]--;
			}
		}
		printf("%d ---- ----\n", counter);
		i++;
	}
	return 0;
}
// 1 3 18 1 2 3 4 1 1 1 1 2 3 4 1 1 1 1 2 3 4