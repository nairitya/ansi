#include <stdio.h>

int main(){
	int t,i=0;
	long long int X, Y;
	scanf("%d", &t);
	while(i<t){
		scanf("%llu%llu", &X,&Y);
		int res = 0;
		if(X == 0 && Y == 0)
			res = 1;
		else if(X<0 && X%2 == 0 && Y>=X && Y <= (-1)*X)
			res = 1;
		else if(X>0 && X%2!=0 && (Y >= (1-X) && Y <= X+1))
			res = 1;
		else if(Y%2==0 && X <= (Y-1) && X >= (-1)*Y)
			res = 1;
		else if(Y<0 && Y%2==0 && X>=Y && X <= (-1)*(Y-1))
			res = 1;
		else res = 0;
	
		if(res)
			printf("YES\n");
		else
			printf("NO\n");
		i++;
	}
	return 0;
}