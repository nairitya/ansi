#include <stdio.h>
#include <stdlib.h>

int main(){
	int t, n, k, i=0;
	scanf("%d", &t);
	while(i<t){
		scanf("%d%d", &n,&k);
		int *a = malloc(sizeof(*a)*n);
		int j, count=0;
		for(j=0;j<n;j++){
			scanf("%d", &a[j]);
			if(a[j]%2 == 0){
				count++;
			}
		}

		if(k == 0 && count == n)
			printf("NO\n");
		else if(count >= k)
			printf("YES\n");
		else 
			printf("NO\n");
		i++;
	}
	return 0;
}