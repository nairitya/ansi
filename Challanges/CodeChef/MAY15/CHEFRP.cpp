#include <iostream>
#include <cstdlib>
using namespace std;

int helper(int n, int *a, int index)
{
  int i=n-1;
  if(i == 0){
    if(i == index)
    return 2;
    else
      return a[i];
  }
    
    else{
      if(i == index)
        return (2 + helper(i, a, index));
      else
        return (a[i] + helper(i, a, index));
    } 
}

void pass_func(int n, int *a, int skip, int index)
{
    if(skip){
      cout<<-1<<endl;
    } else {
      if(n == 1 && a[0]>=2)
        cout<<2<<endl;
      else
      {
        cout<<helper(n, a, index)<<endl;
      }
      
    }
}

int main()
{
  int t, n;
  cin>>t;
  while(t--)
  {
    cin>>n;
    int *a = new int[n];
    int skip = 0;
    int min = 100000, index=0;
    
    for(int i=0;i<n;i++)
    {
      cin>>a[i];
      if(a[i] < min){
        min = a[i];
        index = i;
      }
      if(a[i] < 2)
        skip = 1;
    }
    //cout<<"index is "<<index<<endl;
    pass_func(n, a, skip, index);
    free(a);
  }
}