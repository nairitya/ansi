#include <iostream>
using namespace std;

int main(){
    int t;
    cin>>t;
    while(t--){
	int n;
	cin>>n;
	long long int *m = new long long int[n];
	for(int i=0;i<n;i++)
    	   cin>>m[i];
	long long int total = m[0];
	for(int i=1;i<n;i++){
	   long long int loc = m[i] - m[i-1];
	   if(loc > 0)
		   total += loc;
	}
	cout<<total<<endl;
    }
    return 0;
}
