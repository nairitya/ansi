#include <iostream>
#include <cmath>
#include <algorithm>
using namespace std;

int main(){
	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;

		int temp = n;
		long long int *total = new long long int[n];
		while(temp--){
			int m;
			cin>>m;
			long long int sum = 0;
			for(int i=0;i<2*m;i++){
				int a;
				cin>>a;
				sum += abs(a);
			}
			total[n-temp-1] = sum;
		}
	    for(int i=0;i<n;i++){
	    	int sum=0;
			for(int j=0;j<n;j++){
				if(total[i] > total[j])
					sum += 1;
			}	
			cout<<sum<<" ";
		}
		cout<<endl;
	}
	return 0;    
}
