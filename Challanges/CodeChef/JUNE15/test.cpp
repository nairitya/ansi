#include <iostream>
#include <list>
using namespace std;

class graph{
public:
	int V;
	list<int> *adj;
	void add_edge(int a, int b);
	graph(int v);
	void bfs(int a);
};

graph::graph(int v){
	this->V = v;
	this->adj = new list<int>[v];
}

void graph::add_edge(int a, int b){
	adj[a].push_back(b);
}

void graph::bfs(int s){
	bool *visited = new bool[V];
	for(int i=0;i<V;i++)
		visited[i]=false;
	list<int>queue;
	queue.push_back(s);
	visited[s] = true;
	list<int>::iterator i;
	while(!queue.empty()){
		s = queue.front();
		cout<<s<<" ";
		queue.pop_front();

		for(i=adj[s].begin(); i!=adj[s].end(); i++){
			if(!visited[*i]){
				visited[*i] = true;
				queue.push_back(*i);
			}
		}

	}
}

int main(){
	graph g(5);
	g.add_edge(0, 1);
	g.add_edge(0, 2);
	g.add_edge(1, 2);
	g.add_edge(2, 0);
	g.add_edge(2, 3);
	g.add_edge(4, 3);
	g.add_edge(2, 4);
	g.bfs(2);
	return 0;
}
