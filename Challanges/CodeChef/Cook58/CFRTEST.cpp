#include <cstdio>
using namespace std;

int main(){
	int t, day[50], day_occupied[50];
	scanf("%d", &t);

	while(t--){
		int n;
		scanf("%d", &n);

		for(int i=0;i<n;i++) scanf("%d", &day[i]);
		for(int i=0;i<50;i++) day_occupied[i] = 0;

		int temp = 0;
		for(int i=0;i<n;i++)
			if(day_occupied[day[i] - 1] == 0){
				temp+=1;
				day_occupied[day[i] - 1] = 1;
			}
		printf("%d\n", temp);
	}
	return 0;
}