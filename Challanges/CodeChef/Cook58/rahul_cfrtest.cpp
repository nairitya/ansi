#include<bits/stdc++.h>
using namespace std;

#define take(n) scanf("%d",&n)
#define sl(n) scanf("%lld",&n)
#define p(n) printf("%d",n)
#define ps() printf("%d\n",n)
#define repeat(i,p,q) for(int i=p;i<=q;i++)

int main() 
{
	int t;
 	take(t);
 	while(t--)
 	{
 		set<int>myset;
 		int n;
 		take(n);
 		repeat(i,1,n)
 		{
 			int k;
 			take(k);
 			myset.insert(k);
 		}
 		cout<<myset.size()<<endl;
 	}
}