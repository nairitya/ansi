#include <cstdio>
#include <cstdlib>

using namespace std;

int main(){
	int t;
	scanf("%d", &t);

	while(t--){
		int n;
		scanf("%d", &n);
		int *f = new int[n];
		int *he_coming = new int[n];
		int *r = new int[n];

		for(int i=0;i<n;i++){
			scanf("%d", &f[i]);
			f[i] = f[i] - 1;
			he_coming[i] = 0;
		}

		for(int i=0;i<n;i++)
			scanf("%d", &r[i]);

		int temp=0;
		for(int i=0;i<n;i++){

			if(he_coming[i] == 0){
				if(he_coming[f[i]] == 0){
					if((r[i] + r[f[i]]) > 0){
						temp += (r[i]+r[f[i]]);
						he_coming[i] = 1;
						he_coming[f[i]] = 1;
					}

				}
				else{
					if(r[i] > 0){
						temp += r[i];
						he_coming[i] = 1;
					}

				}	
			}
		}
//b is a;s friend but b has -ve and a+b >0: if a is c's friend and and a+c > a+b Then thing is obvious now. 


		printf("%d\n",temp);
		free(f);
		free(he_coming);
		free(r);
	}

	return 0;
}