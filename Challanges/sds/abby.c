#include <stdio.h>
#include <stdlib.h>

int power_two(long long int x){
	return x && (!(x&(x-1)));
}

int main(){
	int t;
	long long int n;
	
	scanf("%d", &t);
	int count;
	int arr[] = {1024, 1048576};
	while(t--){
		count = 0;
		scanf("%llu", &n);
		
		if(n == 1 || n == 0){
			printf("True\n");
		}
		
		else{
			count = power_two(n);
		
		switch(count){
			case 1:
				printf("True\n");
				break;
			case 2:
				printf("False\n");
				break;
		}
		}
	}
	return 0;
}
