#include <stdio.h>
#include <stdlib.h>
#include <float.h>

void process_this(int n, int k, int x){
	int diff;
	diff = n - k;
	double ans;
	
		
	//printf("K is %d, n is %d\n", k, n);
	// ans = 1000000/k;
	
	if(diff == 0){
		ans = (double)1/k;
		ans *= 1000000;
		// printf("%.6f\n", ((signed long)(ans * 1000000) * 0.000001f));
		printf("%lf\n", ans);
		return;
	}
	
	else{

		if(x<k){
			ans = (double) 1000000/(k*(x+k));
			printf("%lf\n", ans);
			return;
		}

		else if(x > n-k){
			if(x+k > n){
				ans = (double) 1000000/(k*(n-x+k));
				printf("%lf\n", ans);
				return;
			}
			else{
				ans = (double) 1000000/(k*(k+k));
				printf("%lf\n", ans);
				return;
			}
		}

		ans = (double) 1/(k*(diff + 1));
		ans *= 1000000;
		// printf("%.6f\n", ((signed long)(ans * 1000000) * 0.000001f));
        printf("%lf\n", ans);
        return;
	}

}

int main(){
	int t, N, k, x;
	scanf("%d", &t);
	while(t--){
		scanf("%d%d%d", &N, &k, &x);
		process_this(N, k, x);
	}
	return 0;
}
