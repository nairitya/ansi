#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int main(){
	int t, count;
	size_t i;
	char *found, *b, *w;
	b = malloc(sizeof(char)*26);
	w = malloc(sizeof(char)*100);
	scanf("%d", &t);
	while(t--){
		count = 0;
		scanf("%s%s", b, w);
		// fgets(b, sizeof(b), stdin);
		// fgets(w, sizeof(w), stdin);
		i = strlen(b);
		while(i--){
			// printf("%lu\n", i);
			found = strchr(w, b[i]);
			if(found != NULL)
				count++;
		}
		printf("%d\n", count);
	}
	return 0;
}