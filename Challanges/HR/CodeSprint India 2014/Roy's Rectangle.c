#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

void Quad(long long int t1, long long int t2, long long int t3, long long int t4){
    long long int low = (t1 < t2) ? t1 : t2;
    low = (low < t3) ? low : t3;
    low = (low < t4) ? low : t4;
    printf("%llu\n", low);
}

int main() {
    int t;
    long long int x, y, x1, x2, y1, y2, t1, t2, t3, t4;
    scanf("%d", &t);
    while(t--){
        scanf("%llu%llu%llu%llu%llu%llu", &x, &y, &x1, &y1, &x2, &y2);
        t1 = x - x1;
        t2 = x2 - x;
        
        t3 = y - y1;
        t4 = y2 - y;
        
        Quad(t1, t2, t3, t4);
    }
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */    
    return 0;
}