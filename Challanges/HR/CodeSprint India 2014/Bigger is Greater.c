#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

void swap_(char *a, char *b){
	char temp = *a;
	*a = *b;
	*b = temp;
}

void process(char *w, int le){
	if(le < 1){
		printf("no answer\n");
		return;
	}
	else{
		if(w[le] > w[le-1]){
			swap_(&w[le], &w[le-1]);
			printf("%s\n", w);
			return;
		}
		else 
			return process(w, le-1);
	}
}

int main(){
	int t, le;
	scanf("%d", &t);
	char *w = malloc(sizeof(char)*100);

	while(t--){
		scanf("%s", w);
		le = strlen(w);
		process(w, le-1);
	}
	return 0;
}