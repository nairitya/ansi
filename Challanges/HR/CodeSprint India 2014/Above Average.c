#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int main(){
	int t, n, i;
	unsigned int sum, count;
	scanf("%d", &t);
	while(t--){
		i = sum = count = 0;
		scanf("%d", &n);
		int *N = malloc(sizeof(int)*n);
		while(i<n){
			scanf("%d", &N[i]);
			sum += N[i];
			i++;
		}
		sum /= n;
		while(n--){
			if(N[n] > sum)
				count++;
		}
		if(N[n] > sum)
			count++;
		printf("%u\n", count);
	}
	return 0;
}